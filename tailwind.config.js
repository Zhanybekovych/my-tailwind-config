/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    screens: {
      sm: "480",
      md: "768",
      lg: "976",
      xl: "1440",
    },
    extend: {
      colors: {
        primaryAccent: "hsl(120,90%,28%)",
      },
      fontFamily: {
        sans: ['"Poppins"', "sans-serif"],
        serif: ['"Roboto Slab"', "serif"],
      },
    },
  },
  plugins: [],
};
